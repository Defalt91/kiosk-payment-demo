﻿using System;

namespace KioskMachine.Models
{
     public class PaymentModel
    {
        public long AccountId { get; set; }
        public string UserName { get; set; }
        public float Debt { get; set; }
        public float Paid { get; set; }
        public string Today
        {
            get
            {
                return DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            }
        }
    }
}
