﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KioskMachine.Models
{
    public class AccountModel
    {
        public string User { get; set; }
        public float Debt { get; set; }
        public string Message { get; set; }
    }
}
