﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using KioskMachine.Models;

namespace KioskMachine.Libraries
{
    public class GetAccount
    {
        public static async Task<AccountModel> LoadUser(long userNumber = 0)
        {
            string url = "";

            if (userNumber > 0)
            {
                url = $"https://api.xenterglobal.com:2053/account_balance?token=85bd6494458812547d4b282d0924f240&account={ userNumber }";
            }
            else
            {
                throw new Exception("No puede ir con campos vacios");
            }

            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    AccountModel account = await response.Content.ReadAsAsync<AccountModel>();

                    return account;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
            //hay que cerrar el api aqui 
        }
    }
}
