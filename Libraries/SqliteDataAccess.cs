﻿using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using KioskMachine.Models;

namespace KioskMachine.Libraries
{
    public class SqliteDataAccess
    {
        public static List<PaymentModel> LoadPeople()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<PaymentModel>("select * from payment_history", new DynamicParameters());
                return output.ToList();
            }
        }

        public static void SavePayment(PaymentModel person)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("insert into payment_history (account, customer, debt, paid, date) values (@AccountId, @UserName, @Debt, @Paid, @Today)", person);
            }
        }

        private static string LoadConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
