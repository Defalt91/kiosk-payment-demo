﻿using System.Net.Http;
using System.Net.Http.Headers;

namespace KioskMachine.Libraries
{
    //Todo lo hice static porque nomas quiero una instancia a la vez
    public static class ApiHelper
    {
        public static HttpClient ApiClient { get; set; }

        public static void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //Aqui realmente nomas me importa los datos que sean tipo application/json asi que lo estoy filtrando.
            //En caso que llamemos a una pagina web completa no me cargue el resto de sus componentes
        }
    }
}
