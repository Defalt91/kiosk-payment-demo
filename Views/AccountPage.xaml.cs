﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using KioskMachine.Libraries;

namespace KioskMachine.Views
{
    /// <summary>
    /// Interaction logic for AccountPage.xaml
    /// </summary>
    public partial class AccountPage : UserControl
    {
        public AccountPage()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }
        private async Task LoadUser(long accountNumber = 0)
        {
            var account = await GetAccount.LoadUser(accountNumber);

            if (account.User != null)//Revisar si cuenta existe..
            {
                LabelAccount.Content = accountNumber;
                LabelUsers.Content = account.User;
                LabelDebt.Content = account.Debt;
            }
            else
            {
                MessageBox.Show(account.Message, "ALERTA", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private async void searchAccount_Click(object sender, RoutedEventArgs e)
        {
            long AccountNumber = long.Parse(AccountTBox.Text);

            await LoadUser(AccountNumber);
        }

        private void AccountTBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ContinueBtn_Click(object sender, RoutedEventArgs e)
        {
            //DataContext = new BalanceViewModel();
            //setView();
        }
    }
}
