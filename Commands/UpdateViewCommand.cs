﻿using System;
using System.Windows.Input;
using KioskMachine.ViewModels;

namespace KioskMachine.Commands
{
    public class UpdateViewCommand : ICommand
    {
        private MainViewModel viewModel;

        public UpdateViewCommand(MainViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
            //throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            Console.WriteLine("Heeeelellllllooooo");
            switch (parameter.ToString())
            {
                case "Welcome":
                    viewModel.SelectedViewModel = new WelcomeViewModel();
                    break;
                case "Account":
                    viewModel.SelectedViewModel = new AccountViewModel();
                    break;
                case "Balance":
                    viewModel.SelectedViewModel = new BalanceViewModel();
                    break;
                case "Payment":
                    viewModel.SelectedViewModel = new PaymentViewModel();
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
